package benchmark

import org.openjdk.jmh.annotations.Benchmark
import org.openjdk.jmh.annotations.BenchmarkMode
import org.openjdk.jmh.annotations.Mode
import org.openjdk.jmh.annotations.OutputTimeUnit
import java.util.concurrent.TimeUnit

import mutablemap._
import triemap._

class BenchmarkRunner {

  @Benchmark
  @BenchmarkMode(Array(Mode.Throughput, Mode.AverageTime))
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  def mutableMapSaveAndSortedLookup(): Unit = {
    val somethings = Array(
      SomethingSortable("0fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("1fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567891),
      SomethingSortable("2fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567892),
      SomethingSortable("3fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567813),
      SomethingSortable("4fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567894),
      SomethingSortable("6fbc40c2-98f3-44c5-b37b-9decd8162bed", 1224567891),
      SomethingSortable("5fbc40c2-98f3-44c5-b37b-9decd8162bed", 1233367890),
      SomethingSortable("7fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567894),
      SomethingSortable("6fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234367894),
      SomethingSortable("0fbc42c2-98f3-44c5-b37b-9decd8162bed", 1224567890),
      SomethingSortable("0fbc43c2-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("0fbc44c2-98f3-44c5-b37b-9decd8162bed", 1234597890),
      SomethingSortable("0fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234577890),
      SomethingSortable("1fbc44c4-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("3fbc40c4-98f3-44c5-b37b-9decd8162bed", 1239567890),
      SomethingSortable("4fbc40c5-98f3-44c5-b37b-9decd8162bed", 1234564890),
      SomethingSortable("0fbc40c6-98f3-44c5-b37b-9decd8162bed", 1234565290)
    )

    somethings.map(MutableMapRepository.store)

    MutableMapRepository.all
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.Throughput, Mode.AverageTime))
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  def mutableMapSaveAndNOTSortedLookup(): Unit = {
    val somethings = Array(
      SomethingSortable("0fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("1fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567891),
      SomethingSortable("2fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567892),
      SomethingSortable("3fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567813),
      SomethingSortable("4fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567894),
      SomethingSortable("6fbc40c2-98f3-44c5-b37b-9decd8162bed", 1224567891),
      SomethingSortable("5fbc40c2-98f3-44c5-b37b-9decd8162bed", 1233367890),
      SomethingSortable("7fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567894),
      SomethingSortable("6fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234367894),
      SomethingSortable("0fbc42c2-98f3-44c5-b37b-9decd8162bed", 1224567890),
      SomethingSortable("0fbc43c2-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("0fbc44c2-98f3-44c5-b37b-9decd8162bed", 1234597890),
      SomethingSortable("0fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234577890),
      SomethingSortable("1fbc44c4-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("3fbc40c4-98f3-44c5-b37b-9decd8162bed", 1239567890),
      SomethingSortable("4fbc40c5-98f3-44c5-b37b-9decd8162bed", 1234564890),
      SomethingSortable("0fbc40c6-98f3-44c5-b37b-9decd8162bed", 1234565290)
    )

    somethings.map(MutableMapRepository.store)

    MutableMapRepository.allNonSorted
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.Throughput, Mode.AverageTime))
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  def immutableTrieMapSaveAndSortedLookup(): Unit = {
    val somethings = Array(
      SomethingSortable("0fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("1fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567891),
      SomethingSortable("2fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567892),
      SomethingSortable("3fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567813),
      SomethingSortable("4fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567894),
      SomethingSortable("6fbc40c2-98f3-44c5-b37b-9decd8162bed", 1224567891),
      SomethingSortable("5fbc40c2-98f3-44c5-b37b-9decd8162bed", 1233367890),
      SomethingSortable("7fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567894),
      SomethingSortable("6fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234367894),
      SomethingSortable("0fbc42c2-98f3-44c5-b37b-9decd8162bed", 1224567890),
      SomethingSortable("0fbc43c2-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("0fbc44c2-98f3-44c5-b37b-9decd8162bed", 1234597890),
      SomethingSortable("0fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234577890),
      SomethingSortable("1fbc44c4-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("3fbc40c4-98f3-44c5-b37b-9decd8162bed", 1239567890),
      SomethingSortable("4fbc40c5-98f3-44c5-b37b-9decd8162bed", 1234564890),
      SomethingSortable("0fbc40c6-98f3-44c5-b37b-9decd8162bed", 1234565290)
    )

    somethings.map(TrieMapRepository.store)

    TrieMapRepository.all
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.Throughput, Mode.AverageTime))
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  def immutableTrieMapSaveAndNOTSortedLookup(): Unit = {
    val somethings = Array(
      SomethingSortable("0fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("1fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567891),
      SomethingSortable("2fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567892),
      SomethingSortable("3fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567813),
      SomethingSortable("4fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567894),
      SomethingSortable("6fbc40c2-98f3-44c5-b37b-9decd8162bed", 1224567891),
      SomethingSortable("5fbc40c2-98f3-44c5-b37b-9decd8162bed", 1233367890),
      SomethingSortable("7fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234567894),
      SomethingSortable("6fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234367894),
      SomethingSortable("0fbc42c2-98f3-44c5-b37b-9decd8162bed", 1224567890),
      SomethingSortable("0fbc43c2-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("0fbc44c2-98f3-44c5-b37b-9decd8162bed", 1234597890),
      SomethingSortable("0fbc40c2-98f3-44c5-b37b-9decd8162bed", 1234577890),
      SomethingSortable("1fbc44c4-98f3-44c5-b37b-9decd8162bed", 1234567890),
      SomethingSortable("3fbc40c4-98f3-44c5-b37b-9decd8162bed", 1239567890),
      SomethingSortable("4fbc40c5-98f3-44c5-b37b-9decd8162bed", 1234564890),
      SomethingSortable("0fbc40c6-98f3-44c5-b37b-9decd8162bed", 1234565290)
    )

    somethings.map(TrieMapRepository.store)

    TrieMapRepository.allNonSorted
  }
}
