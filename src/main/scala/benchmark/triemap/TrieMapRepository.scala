package benchmark
package triemap

import scala.collection.concurrent.TrieMap

object TrieMapRepository {
  lazy val storage = TrieMap.empty[String, SomethingSortable]

  def store(ss: SomethingSortable): SomethingSortable = {
    storage += ((ss.id, ss))
    ss
  }

  def all: Array[SomethingSortable] = storage.values.toArray.sortWith((a,b) => a.createdAt < b.createdAt)

  def allNonSorted: Array[SomethingSortable] = storage.values.toArray
}
