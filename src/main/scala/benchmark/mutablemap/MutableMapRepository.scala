package benchmark
package mutablemap

import scala.collection.mutable.Map

object MutableMapRepository {
  lazy val storage = Map.empty[String, SomethingSortable]

  def store(ss: SomethingSortable): SomethingSortable = {
    storage += ((ss.id, ss))
    ss
  }

  def all: Array[SomethingSortable] = storage.values.toArray.sortWith((a,b) => a.createdAt < b.createdAt)

  def allNonSorted: Array[SomethingSortable] = storage.values.toArray
}
