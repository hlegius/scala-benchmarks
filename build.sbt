name := "Benchmarking mutable.Map vs concurrent.TrieMap over objects"

scalaVersion := "2.12.4"
enablePlugins(JmhPlugin)
