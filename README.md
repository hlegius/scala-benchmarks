# Scala (Personal/Study-only) Benchmarks

Collect a few Scala benchmarks for study purposes.

## Cases:

## mutable.Map (non-thread-safe) vs concurrent.TrieMap (thread-safe)

**Goal**: In-Memory storage for POC, Fake Storage, Pet Projects, etc.

**Runner**: `src/main/scala/BenchmarkRunner.scala`

### Output (using `jmh: run`)

Single Threaded; 20 warmups; 20 iterations each for Throughput and Average Time (Microseconds)

```
# Intel Core i5 Dual Core 6360U @ 2Ghz

[info] # Run complete. Total time: 01:00:52
[info] Benchmark                                                Mode  Cnt  Score   Error   Units
[info] BenchmarkRunner.immutableTrieMapSaveAndNOTSortedLookup  thrpt  200  0.509 ± 0.004  ops/us
[info] BenchmarkRunner.immutableTrieMapSaveAndSortedLookup     thrpt  200  0.435 ± 0.001  ops/us
[info] BenchmarkRunner.mutableMapSaveAndNOTSortedLookup        thrpt  200  1.586 ± 0.011  ops/us
[info] BenchmarkRunner.mutableMapSaveAndSortedLookup           thrpt  200  0.953 ± 0.017  ops/us
[info] BenchmarkRunner.immutableTrieMapSaveAndNOTSortedLookup   avgt  200  1.943 ± 0.005   us/op
[info] BenchmarkRunner.immutableTrieMapSaveAndSortedLookup      avgt  200  2.314 ± 0.022   us/op
[info] BenchmarkRunner.mutableMapSaveAndNOTSortedLookup         avgt  200  0.637 ± 0.003   us/op
[info] BenchmarkRunner.mutableMapSaveAndSortedLookup            avgt  200  1.007 ± 0.007   us/op

[info] # JMH version: 1.19
[info] # VM version: JDK 1.8.0_144, VM 25.144-b01
[info] # VM invoker: /Library/Java/JavaVirtualMachines/jdk1.8.0_144.jdk/Contents/Home/jre/bin/java
[info] # VM options: <none>
[info] # Warmup: 20 iterations, 1 s each
[info] # Measurement: 20 iterations, 1 s each
[info] # Timeout: 10 min per iteration
[info] # Threads: 1 thread, will synchronize iterations

[info] Result "benchmark.BenchmarkRunner.immutableTrieMapSaveAndNOTSortedLookup":
[info]   0.509 ±(99.9%) 0.004 ops/us [Average]
[info]   (min, avg, max) = (0.390, 0.509, 0.528), stdev = 0.018
[info]   CI (99.9%): [0.504, 0.513] (assumes normal distribution)
[info] Result "benchmark.BenchmarkRunner.immutableTrieMapSaveAndSortedLookup":
[info]   0.435 ±(99.9%) 0.001 ops/us [Average]
[info]   (min, avg, max) = (0.419, 0.435, 0.447), stdev = 0.005
[info]   CI (99.9%): [0.433, 0.436] (assumes normal distribution)
[info] Result "benchmark.BenchmarkRunner.mutableMapSaveAndNOTSortedLookup":
[info]   1.586 ±(99.9%) 0.011 ops/us [Average]
[info]   (min, avg, max) = (1.457, 1.586, 1.677), stdev = 0.048
[info]   CI (99.9%): [1.575, 1.597] (assumes normal distribution)
[info] Result "benchmark.BenchmarkRunner.mutableMapSaveAndSortedLookup":
[info]   0.953 ±(99.9%) 0.017 ops/us [Average]
[info]   (min, avg, max) = (0.724, 0.953, 1.047), stdev = 0.072
[info]   CI (99.9%): [0.936, 0.970] (assumes normal distribution)
[info] Result "benchmark.BenchmarkRunner.immutableTrieMapSaveAndNOTSortedLookup":
[info]   1.943 ±(99.9%) 0.005 us/op [Average]
[info]   (min, avg, max) = (1.891, 1.943, 2.007), stdev = 0.022
[info]   CI (99.9%): [1.938, 1.948] (assumes normal distribution)
[info] Result "benchmark.BenchmarkRunner.immutableTrieMapSaveAndSortedLookup":
[info]   2.314 ±(99.9%) 0.022 us/op [Average]
[info]   (min, avg, max) = (2.231, 2.314, 3.426), stdev = 0.094
[info]   CI (99.9%): [2.292, 2.336] (assumes normal distribution)
[info] Result "benchmark.BenchmarkRunner.mutableMapSaveAndNOTSortedLookup":
[info]   0.637 ±(99.9%) 0.003 us/op [Average]
[info]   (min, avg, max) = (0.611, 0.637, 0.682), stdev = 0.011
[info]   CI (99.9%): [0.634, 0.639] (assumes normal distribution)
[info] Result "benchmark.BenchmarkRunner.mutableMapSaveAndSortedLookup":
[info]   1.007 ±(99.9%) 0.007 us/op [Average]
[info]   (min, avg, max) = (0.947, 1.007, 1.100), stdev = 0.030
[info]   CI (99.9%): [1.000, 1.014] (assumes normal distribution)
```

### Conclusion
`mutable.Map` is more than 2x faster (and 2x more throughput) than TrieMap. TrieMap in other hand, is thread-safe
and the implementation is quite similar to the Map if you have a defined comparison function.

`concurrent.TrieMap` is hashcode ordered by default; `mutable.Map`, insertion ordered.

I am using `Array` instead of Set/List due [this Scala collections' benchmark](http://www.lihaoyi.com/post/BenchmarkingScalaCollections).


## Disclaimer

Don't trust me. Bench by yourself. Clone or build your own benchmarks. It's fun!